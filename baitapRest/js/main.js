
const $ = document.querySelector.bind(document)

averageScore = (...element) => {
    let n = 0;
    return element.map(element => { n += element }), (n / element.length).toFixed(2);
}

document.querySelector('#btnKhoi1').onclick = () => {
    let maths = $('#inpToan').value * 1
    let physical = $('#inpLy').value * 1
    let Chemistry = $('#inpHoa').value * 1

    $('#tbKhoi1').innerHTML = averageScore(maths, physical, Chemistry)
}

document.querySelector('#btnKhoi2').onclick = () => {
    let literary = $('#inpVan').value * 1
    let history = $('#inpSu').value * 1
    let geography = $('#inpDia').value * 1
    let english = $('#inpEnglish').value * 1

    $('#tbKhoi2').innerHTML = averageScore(literary, history, geography, english)
}

