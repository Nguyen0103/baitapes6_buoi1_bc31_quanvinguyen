
const colorList = ['pallet', 'viridian', 'pewter', 'cerulean', 'vermillion', 'lavender', 'celadon', 'saffron', 'fuschia', 'cinnabar'];
let container = document.getElementById("colorContainer");
let colorButton = document.getElementsByClassName("color-button");
let house = document.getElementById("house");

renderUI = () => {
    for (let i = 0; i < colorList.length; i++)
        container.innerHTML += 0 == i
            ? "<button class='color-button " + colorList[i] + " active'></button>"
            : "<button class='color-button " + colorList[i] + "'></button>"
}, renderUI();


changeColor = (color, index) => {
    for (let color = 0; color < colorButton.length; color++)
        colorButton[color].classList.remove('active');
    colorButton[index].classList.add('active');
    house.className = 'house ' + color;

}

for (let i = 0; i < colorButton.length; i++)
    colorButton[i].addEventListener('click',
        () => {
            changeColor(colorList[i], i)
        }
    )



